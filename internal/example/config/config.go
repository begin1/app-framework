package config

import (
	"gitlab.com/begin1/app-framework"
	"gitlab.com/begin1/library/database/etcd"
	"gitlab.com/begin1/library/database/mongo"
	"gitlab.com/begin1/library/database/redis"
	"gitlab.com/begin1/library/database/sql"
	"gitlab.com/begin1/library/kafka"
	"gitlab.com/begin1/library/net/httpclient"
	"gitlab.com/begin1/library/net/redlock"
	"gitlab.com/begin1/library/queue"
)

var (
	// ====================
	// >>>请勿删除<<<
	//
	// 全局配置文件
	// ====================
	Conf *Config
)

// ====================
// 自定义配置文件
//
// 根据实际情况，选择性添加
// ====================

// host配置文件
type HostsConfig struct {
	Github   string `yaml:"github"`
	UserAuth string `yaml:"userAuth"`
}

// ====================
// >>>请勿删除<<<
//
// 配置文件
// ====================
type Config struct {
	// ====================
	// >>>请勿删除<<<
	//
	// 基础配置文件
	// ====================
	*framework.Config `yaml:",inline"`

	// ====================
	// 根据实际情况，选择性保留
	// ====================
	// 会员Mongo配置文件
	VipMongo *mongo.Config `yaml:"vipMongo"`
	// 支付Mongo配置文件
	PayMongo *mongo.Config `yaml:"payMongo"`
	// Http客户端配置文件
	HttpClient *httpclient.Config `yaml:"httpClient"`
	// Mysql配置文件
	Mysql *sql.Config `yaml:"mysql"`
	// Redis配置文件
	Redis *redis.Config `yaml:"redis"`
	// Redlock配置文件
	Redlock *redlock.Config `yaml:"redlock"`
	// Etcd配置文件
	Etcd *etcd.Config `yaml:"etcd"`
	// Kafka生产者配置文件
	KafkaProducer *kafka.Config `yaml:"kafkaProducer"`
	// AMQP生产者配置文件
	AMQPProducer *queue.Config `yaml:"amqpProducer"`

	// host配置文件
	Host *HostsConfig `yaml:"host"`
}

// ====================
// >>>请勿删除<<<
//
// 读取配置文件
//
//		configPath为配置文件的所属位置，例如 ./config/config.yaml
// ====================
func Read(configPath string) *Config {
	Conf = new(Config)

	// 解码yaml配置文件
	framework.DecodeConfig(configPath, Conf)

	return Conf
}
