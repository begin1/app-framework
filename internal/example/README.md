# 示例

## 基础说明

1. 该示例包含该框架的基础使用方式及部分使用标准
2. 在http服务接口示例中，包含大量常规场景下的使用标准
3. Golang 版本要求最低 1.13
4. 本框架内部基于Library库，请查看[Library文档](https://gitlab.com/begin1/library)
5. 如有其他问题，欢迎提[Issue](https://gitlab.com/begin1/app-framework/issues/new)

## 环境变量说明

1. 使用该库需要设置如下环境变量
   ```
   GOPROXY=http://goproxy.qingting-hz.com
   
   GOSUMDB=off
   ```

## 目录结构
   ```
   .
   ├── .gitignore                       git忽略文件
   ├── Dockerfile                       Docker构建文件
   ├── cmd                              主程序包
   │   ├── amqp                         AMQP程序包
   │   │   └── amqp.go                  AMQP服务主程序
   │   ├── http                         Http程序包
   │   │   └── http.go                  Http服务主程序
   │   ├── job                          定时任务程序包
   │   │   └── job.go                   定时任务服务主程序
   │   ├── kafka                        Kafka程序包
   │   │   └── kafka.go                 Kafka服务主程序
   │   └── rpc                          RPC程序包
   │       └── rpc.go                   RPC服务主程序
   ├── config                           配置文件目录包
   │   ├── config.go                    基础配置代码
   │   └── config.yaml                  示例配置文件
   ├── dao                              数据库层包
   │   └── dao.go                       基础数据库代码
   ├── models                           模型包
   │   ├── entity                       实体模型包
   │   ├── req                          请求模型包
   │   └── resp                         响应模型包
   ├── server                           服务器包
   │   ├── http                         Http服务器包
   │   │   ├── handler                  Http服务器接口处理包
   │   │   └── server.go                Http服务器基础代码
   │   ├── job                          任务服务器包
   │   │   └── server.go                RPC服务器基础代码
   │   ├── rpc                          RPC服务器包
   │   │   └── server.go                RPC服务器基础代码
   │   └── subscribe                    订阅服务器包
   │       ├── amqp.go                  AMQP服务器基础代码
   │       └── kafka.go                 Kafka服务器基础代码
   └── service                          服务包
   ```  
  
## 框架本地启动说明

1. 将完整的配置文件`config.yaml`拷贝至当前运行目录的`config`文件夹下
2. 启动服务

【**重要**】注意项目仓库中不应保存任何环境配置文件，包括本地配置。

【**注意**】完整配置文件是指包含密码等私有配置的文件，而远程配置中心的生成文件中并不会显式展示密码。

## 框架使用说明

1. 使用git创建新项目，假设项目名为 framework-example
2. 设置GOPROXY及GOSUMDB环境变量
3. 使用go mod init初始化项目
   ```
   // 请将 framework-example 替换为真实项目名
   go mod init framework-example
   ```
4. 获取框架,其中vx.x.x代表版本号
   ```
   go get gitlab.com/begin1/library@vx.x.x
   go get gitlab.com/begin1/app-framework@vx.x.x
   ```
5. 参考当前示例，在新项目根目录，创建cmd,config,models,server,service,dao包
6. config包

    1. 拷贝config包下config.go到新项目config包下。config.yaml示例配置文件。
    2. 【**重要**】根据所需组件，选择性更改或删除`config.go`文件下的配置文件。注意项目仓库中不应保存任何配置文件。
    3. 【**重要**】配置文件需要在[配置中心](https://git2.qingtingfm.com/infra/config-center)提交基础配置文件，并通过MergeRequest合并至master使用，详情请参考配置中心ReadMe。
    4. 【**注意**】配置文件可参考当前项目的`config.yaml`以及远程配置中心。

7. dao包
    
    1. 拷贝`dao.go`文件到新项目dao包下
    2. 【**重要**】改变import饮用，将config引用修改成本地项目
       ```
       改变前:
       import "gitlab.com/begin1/app-framework/internal/example/config"
       
       // 请将 framework-example 替换为真实项目名
       改变后:
       import "framework-example/config"
       ```
    3. 根据所需组件，选择性更改或删除`dao.go`文件下的组件
    
8. models包
    
    1. 在新项目models包下创建entity,req,resp包
    
9. service包

    1. 拷贝`service.go`文件到新项目service包下
    2. 【**重要**】改变import饮用，将config及dao引用修改成本地项目
       ```
       改变前:
       import (
            "gitlab.com/begin1/app-framework/internal/example/config"
            "gitlab.com/begin1/app-framework/internal/example/dao"
       )
       
       // 请将 framework-example 替换为真实项目名
       改变后:
       import (
            "framework-example/config"
            "framework-example/dao"
       )
       ```
    3. 根据所需组件，选择性更改或删除`service.go`文件下的组件
    
10. server包

    1. 根据所需服务，在新项目server包下，添加相应的http/rpc/subscribe包，并拷贝其基础代码，通常为`server.go`文件
    2. 以http服务为例，在新项目http包下创建handler包
    3. 【**重要**】改变import饮用，将handler引用修改成本地项目
       ```
       改变前:
       import (
            "gitlab.com/begin1/app-framework/internal/example/server/http/handler"
       )
       
       // 请将 framework-example 替换为真实项目名
       改变后:
       import (
            "framework-example/server/http/handler"
       )
       ```
    4. 根据实际情况，在基础代码中，修改路由及中间件等，并添加handler方法
    5. 【**重要**】在引入service或models等包时，请引入本地包，请勿引入example中的包
       ```
       错误:
       import (
	        "gitlab.com/begin1/app-framework/internal/example/models/req"
	        "gitlab.com/begin1/app-framework/internal/example/service"
       )
       
       // 请将 framework-example 替换为真实项目名
       正确:
       import (
	        "framework-example/models/req"
	        "framework-example/service"
       )
       ```    

11. 主程序

    1. 根据所需服务，拷贝主程序文件(`amqp.go`/`http.go`/`job.go`/`kafka.go`/`rpc.go`)到新项目cmd包下
    2. 【**重要**】改变import饮用
       ```
       错误:
       import (
	        "gitlab.com/begin1/app-framework/internal/example/config"
	        "gitlab.com/begin1/app-framework/internal/example/server/http"
	        "gitlab.com/begin1/app-framework/internal/example/service"
       )
       
       // 请将 framework-example 替换为真实项目名
       正确:
       import (
	        "framework-example/config"
	        "framework-example/server/http"
	        "framework-example/service"
       )
       ```
    
12. 其他

    1. 【**重要**】拷贝`.gitignore`,`Dockerfile` 文件，并根据实际文件结构修改`Dockerfile`文件

## 提示

1. 为了避免自动引入错误的包，建议关闭自动引用
   ```
   例如：
   使用GoLand时，请关闭 `add unambiguous imports on the fly`
   ```
2. 如项目包含多个启动程序，可通过修改 `Tars`环境管理中的执行命令 / `AMS`发布任务中的启动命令 配置。
   ```
   如该示例中包含5个启动程序，若需要启动`http`程序，则可将命令修改为`./http`，如需要启动`job`程序，则修改为`./job`
   ```