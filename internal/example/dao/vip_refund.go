package dao

import (
	"context"
	"encoding/json"
	"gitlab.com/begin1/app-framework/internal/example/models/entity"
	"gitlab.com/begin1/library/net/errcode"
	"github.com/pkg/errors"
	"github.com/streadway/amqp"
)

// 发送退款到AMQP
func (d *Dao) SendVipRefundNoticeToAMQP(ctx context.Context, notice *entity.VipRefundNotice) error {
	data, err := json.Marshal(notice)
	if err != nil {
		// ==========================
		// 在首次生成error时，应当立即使用errors.Wrapf包裹
		// 外层只需直接返回error，无需再次包裹
		// ==========================
		return errors.Wrapf(errcode.InternalError, "%s", err)
	}

	// ==========================
	// 发送AMQP消息
	// ==========================
	err = d.AMQPSession.Push(false, false, amqp.Publishing{
		ContentType: "text/plain",
		Body:        data,
	})
	if err != nil {
		// ==========================
		// 在首次生成error时，应当立即使用errors.Wrapf包裹
		// 外层只需直接返回error，无需再次包裹
		// ==========================
		return errors.Wrap(errcode.InternalError, err.Error())
	}

	return nil
}
