package handler

import (
	"fmt"
	"gitlab.com/begin1/app-framework/internal/example/models/req"
	"gitlab.com/begin1/app-framework/internal/example/service"
	"gitlab.com/begin1/library/net/errcode"
	ginUtil "gitlab.com/begin1/library/net/gin"
	"gitlab.com/begin1/library/net/response"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
)

// 获取github用户仓库handler
func GetOwnerGithubRepos(c *gin.Context) {
	// ==========================
	// 绑定请求
	// 使用v9校验器校验请求：https://godoc.org/gopkg.in/go-playground/validator.v9
	// ==========================
	var getReq req.GetGithubRepositoryListReq
	getReq.Owner = c.Param("owner")
	err := c.ShouldBindWith(&getReq, ginUtil.Query)
	if err != nil {
		response.JSON(c, nil, errors.Wrapf(errcode.InvalidParams, "%s", err))
		return
	}

	// 获取列表
	res, err := service.SVC.GetOwnerGithubReposList(c, &getReq)
	if err != nil {
		response.JSON(c, nil, err)
		return
	}

	// 响应
	response.JSON(c, res, nil)
}

// 获取github用户聚合数据handler
func GetOwnerGithubAggregation(c *gin.Context) {
	// 获取owner
	owner := c.Param("owner")
	if owner == "" {
		response.JSON(c, nil, errors.Wrap(errcode.InvalidParams, fmt.Sprintf("参数不合法:owner")))
		return
	}

	// 获取详情
	res, err := service.SVC.GetOwnerGithubAggregationResp(c, owner)
	if err != nil {
		response.JSON(c, nil, err)
		return
	}

	// 响应
	response.JSON(c, res, nil)
}
