package handler

import (
	"gitlab.com/begin1/app-framework/internal/example/models/req"
	"gitlab.com/begin1/app-framework/internal/example/service"
	"gitlab.com/begin1/library/net/errcode"
	"gitlab.com/begin1/library/net/response"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
)

// 创建会员退款的handler
func CreateVipRefund(c *gin.Context) {
	// 绑定请求
	var createReq req.CreateVipRefundReq
	err := c.ShouldBindJSON(&createReq)
	if err != nil {
		// ==========================
		// 在首次生成error时，应当立即使用errors.Wrapf包裹
		// 外层只需直接返回error，无需再次包裹
		// ==========================
		response.JSON(c, nil, errors.Wrapf(errcode.InvalidParams, "%s", err))
		return
	}

	// 创建资源
	err = service.SVC.CreateRefundNotice(c, &createReq)
	if err != nil {
		// ==========================
		// 在首次生成error时，应当立即使用errors.Wrapf包裹
		// 外层只需直接返回error，无需再次包裹
		// ==========================
		response.JSON(c, nil, err)
		return
	}

	// 响应
	response.JSON(c, nil, nil)
}
