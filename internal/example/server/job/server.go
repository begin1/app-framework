package job

import (
	"context"
	"time"

	framework "gitlab.com/begin1/app-framework"
	"gitlab.com/begin1/app-framework/internal/example/service"
	"github.com/spf13/cast"
)

// ====================
// >>>请勿删除<<<
//
// 获取任务服务器
// ====================
func GetServer() framework.ServerInterface {
	svr := new(framework.JobServer)

	// ====================
	// >>>请勿删除<<<
	//
	// 根据实际情况修改
	// ====================
	// 设置任务函数
	svr.SetJob("random-number", func(ctx context.Context) error {
		for i := 0; i < 30; i++ {
			// 生成随机数
			err := service.SVC.PrintRandomNumber(ctx, i)
			// 若返回错误，则任务结束
			if err != nil {
				return err
			}
			time.Sleep(time.Second)
			cast.ToString("s")
		}
		return nil
	})

	return svr
}
