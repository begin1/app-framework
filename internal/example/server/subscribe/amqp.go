package subscribe

import (
	"context"
	framework "gitlab.com/begin1/app-framework"
	"gitlab.com/begin1/app-framework/internal/example/service"
	_context "gitlab.com/begin1/library/base/context"
	"gitlab.com/begin1/library/log"
	"gitlab.com/begin1/library/net/errcode"
	"gitlab.com/begin1/library/queue"
	uuid "github.com/satori/go.uuid"
)

// ====================
// >>>请勿删除<<<
//
// 获取AMQP消息队列订阅服务器
//
// 该服务器展示了基本的自动回复(ack)订阅服务
//
// 若消费服务无法处理消息或消费服务意外终止/报错时，应当在保证消费函数幂等性的前提下，
// 可以通过 手动回复(ack)的方式，避免消息消费失败或丢失
// ====================
func GetAMQPServer() framework.ServerInterface {
	svr := new(framework.AMQPServer)

	// ====================
	// >>>请勿删除<<<
	//
	// 根据实际情况，更改为配置文件中的队列名
	// ====================
	// 设置队列名
	svr.SetQueueName("app_framework")

	// ====================
	// >>>请勿删除<<<
	//
	// 根据实际情况，更改为配置文件中的会话名
	// ====================
	// 设置会话名
	svr.SetSessionName("refund")

	// ====================
	// >>>请勿删除<<<
	//
	// 设置订阅函数
	// ====================
	svr.SubscribeFunction = func(ctx context.Context, queueConfig *queue.Config, session *queue.Session) error {
		// 获取会话配置
		sessionConfig := queueConfig.Session[svr.GetSessionName()]

		// 获取channel
		channel, err := session.GetChannel()
		if err != nil {
			return err
		}

		// ====================
		// 根据实际情况，更改为配置文件中的会话名
		// ====================
		// 声明队列
		_, err = channel.QueueDeclare(sessionConfig.Name,
			true, false, false, false, nil)
		if err != nil {
			return err
		}

		// ====================
		// 获取简单的自动回复(ack)消费流
		//
		// 若消费服务无法处理消息或消费服务报错时，应当在保证消费函数幂等性的前提下，
		// 增加重试机制或通过手动回复(ack)的方式，避免消息消费失败或丢失
		// ====================
		msgChannel, err := session.SingleConsumeStream()
		if err != nil {
			return err
		}
		// ====================
		// 获取手动回复(ack)消费流
		// ====================
		//msgChannel, err := session.NoAutoAckConsumeStream(20, 0, true)
		//if err != nil {
		//	return err
		//}

		// 遍历消费
		for d := range msgChannel {
			currentContext, cancel := context.WithCancel(
				context.WithValue(ctx, _context.ContextUUIDKey, uuid.NewV4().String()),
			)

			// ====================
			// 消费
			//
			// 根据实际情况，修改消费函数
			// ====================
			err := service.SVC.PrintAMQPMessageBody(currentContext, d.Body)
			if err != nil {
				log.Errorv(currentContext, errcode.GetErrorMessageMap(err))

				// ====================
				// 处理失败，手动拒绝
				// ====================
				//err = d.Reject(true)
				//if err != nil {
				//	log.Errorv(currentContext, errcode.GetErrorMessageMap(err))
				//}
				//continue
			}

			// ====================
			// 处理成功，手动回复
			// ====================
			//err = d.Ack(false)
			//if err != nil {
			//	log.Errorv(currentContext, errcode.GetErrorMessageMap(err))
			//}

			// 消费结束，取消当前context
			cancel()
		}

		return nil
	}

	return svr
}
