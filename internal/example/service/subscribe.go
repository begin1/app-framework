package service

import (
	"context"
	"gitlab.com/begin1/library/log"
	"gitlab.com/begin1/library/net/errcode"
	"github.com/Shopify/sarama"
	"github.com/pkg/errors"
)

// 打印AMQP消息体
func (s *Service) PrintAMQPMessageBody(ctx context.Context, msg []byte) (err error) {
	// 防止panic中断整个程序
	defer func() {
		if e := recover(); e != nil {
			err = errors.Wrapf(errcode.InternalError, "%s", e)
		}
	}()

	log.Infoc(ctx, "Message queue:app_framework session:order message=%s", string(msg))
	return nil
}

// 打印Kafka消息体
func (s *Service) PrintKafkaMessageBody(ctx context.Context, msg *sarama.ConsumerMessage) (err error) {
	// 防止panic中断整个程序
	defer func() {
		if e := recover(); e != nil {
			err = errors.Wrapf(errcode.InternalError, "%s", e)
		}
	}()

	log.Infoc(ctx, "Message topic:%q partition:%d offset:%d message=%s",
		msg.Topic, msg.Partition, msg.Offset, string(msg.Value))
	return nil
}
