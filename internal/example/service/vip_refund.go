package service

import (
	"context"
	"gitlab.com/begin1/app-framework/internal/example/models/entity"
	"gitlab.com/begin1/app-framework/internal/example/models/req"
	"gitlab.com/begin1/library/base/deepcopy.v2"
	"gitlab.com/begin1/library/net/errcode"
	"github.com/pkg/errors"
)

// 创建媒体资源
func (s *Service) CreateRefundNotice(ctx context.Context, createReq *req.CreateVipRefundReq) error {
	// ==========================
	// 将请求模型复制到消息实体中，通过Library中的deepcopy包实现
	// ==========================
	detail := new(entity.VipRefundNotice)
	err := deepcopy.Copy(createReq).To(detail)
	if err != nil {
		// ==========================
		// 在首次生成error时，应当立即使用errors.Wrapf包裹
		// 外层只需直接返回error，无需再次包裹
		// ==========================
		return errors.Wrapf(errcode.InternalError, "%s", err)
	}

	// ==========================
	// 发送amqp消息
	// ==========================
	err = s.dao.SendVipRefundNoticeToAMQP(ctx, detail)
	if err != nil {
		return err
	}

	return nil
}
