module gitlab.com/begin1/app-framework

go 1.13

require (
	github.com/Shopify/sarama v1.24.1
	github.com/gin-gonic/gin v1.6.3
	github.com/jinzhu/gorm v1.9.11
	github.com/pkg/errors v0.8.1
	github.com/satori/go.uuid v1.2.0
	github.com/spf13/cast v1.3.1
	github.com/streadway/amqp v0.0.0-20190827072141-edfb9018d271
	github.com/stretchr/testify v1.4.0
	gitlab.com/begin1/library v0.0.1
	go.mongodb.org/mongo-driver v1.3.3
	google.golang.org/grpc v1.26.0
	gopkg.in/yaml.v3 v3.0.0-20200605160147-a5ece683394c
)
