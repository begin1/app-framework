package framework

import (
	"context"
	"fmt"
	"gitlab.com/begin1/library/goroutine"
	"google.golang.org/grpc"
	"net"
)

// GRPC服务器
type GRPCServer struct {
	// GRPC服务器
	server *grpc.Server
	// 服务名
	name string
	// 配置文件
	config *Config
}

// 实现ServerInterface
func (svr *GRPCServer) ShutDown(ctx context.Context) (err error) {
	// 尝试优雅关闭
	ch := make(chan struct{})
	eg := goroutine.New("RPC")
	eg.Go(ctx, "GracefulStop", func(ctx context.Context) error {
		svr.server.GracefulStop()
		close(ch)
		return nil
	})

	select {
	// 超时，强制关闭
	case <-ctx.Done():
		svr.server.Stop()
		err = ctx.Err()
	case <-ch:
	}

	return
}

// 实现ServerInterface
func (svr *GRPCServer) Start(c *Config, svc ServiceInterface) {
	svr.name = "GRPC"
	svr.config = c
	svr.server = grpc.NewServer()

	listener, err := net.Listen("tcp",
		fmt.Sprintf("%s:%d", c.RPC.Endpoint.Address, c.RPC.Endpoint.Port))
	if err != nil {
		panic(err)
	}
	svc.StartServer(svr.name, func(ctx context.Context) error {
		return svr.server.Serve(listener)
	})
}

// 实现ServerInterface
func (svr *GRPCServer) Name() string {
	return svr.name
}
