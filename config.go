package framework

import (
	"io/ioutil"
	"os"
	"reflect"

	"gitlab.com/begin1/library/base/ctime"
	"gitlab.com/begin1/library/goroutine"
	"gitlab.com/begin1/library/kafka"
	"gitlab.com/begin1/library/log"
	"gitlab.com/begin1/library/net/gin"
	"gitlab.com/begin1/library/net/tracing"
	"gitlab.com/begin1/library/queue"
	"gopkg.in/yaml.v3"
)

// RPC 配置
type RPCConfig struct {
	// 服务地址
	Endpoint struct {
		Address string `yaml:"address"`
		Port    int    `yaml:"port"`
	} `yaml:"endpoint"`
	// 超时时间
	Timeout ctime.Duration `yaml:"timeout"`
}

// 基础配置
type Config struct {
	// 环境变量
	Env string `yaml:"env"`
	// 服务id
	AppID string `yaml:"appID"`
	// 服务请求的超时时间
	// 服务关闭的超时时间
	ShunDownTimeout ctime.Duration `yaml:"shunDownTimeout"`

	// 健康检查路由名
	HealthCheckRouter string `yaml:"healthCheckRouter"`
	// 数据监控路由名
	MetricsRouter string `yaml:"metricsRouter"`
	// 是否关闭pprof路由
	DisablePProf bool `yaml:"disablePProf"`
	// 是否关闭数据统计
	DisableMetrics bool `yaml:"disableMetrics"`
	// 是否关闭链路跟踪
	DisableTracing bool `yaml:"disableTracing"`
	// 是否关闭异常捕获
	DisableCatchPanic bool `yaml:"disableCatchPanic"`
	// 是否关闭流量控制
	DisableTrafficShaping bool `yaml:"disableTrafficShaping"`
	// 限流qps
	TrafficShapingQPS float64 `yaml:"trafficShapingQPS"`
	// 限流并发
	TrafficShapingConcurrency float64 `yaml:"trafficShapingConcurrency"`

	// Http服务配置文件
	Gin *gin.Config `yaml:"gin"`
	// RPC服务配置文件
	RPC *RPCConfig `yaml:"rpc"`
	// AMQP消息队列订阅服务配置文件
	AMQP map[string]*queue.Config `yaml:"amqp"`
	// Kafka消息队列订阅服务配置文件
	Kafka map[string]*kafka.Config `yaml:"kafka"`
	// 日志配置文件
	Log *log.Config `yaml:"log"`
	// goroutine配置文件
	Goroutine *goroutine.Config `yaml:"goroutine"`
	// tracing配置文件
	Trace *tracing.Config `yaml:"trace"`
}

// 从配置文件中解码到相应的配置结构体
// 		configPath为配置文件的所属位置，例如 ./config/config.yaml
//		config为需要解析到的结构体指针
func DecodeConfig(configPath string, config interface{}) {
	if configPath == "" {
		panic("config path is empty")
	}

	if config == nil {
		panic("config is nil")
	}
	if reflect.ValueOf(config).IsNil() {
		panic("config value is nil")
	}
	if reflect.TypeOf(config).Kind() != reflect.Ptr {
		panic("config value is not ptr")
	}

	err := DecodeConfigFromLocal(configPath, config)
	if err != nil {
		panic(err)
	}
}

// 从配置文件中解码到相应的配置结构体
// 		configPath为配置文件的所属位置，例如 ./config/config.yaml
//		config为需要解析到的结构体指针
func DecodeConfigFromLocal(configPath string, config interface{}) error {
	configFile, err := os.Open(configPath)
	if err != nil {
		return err
	}
	defer configFile.Close()

	configData, err := ioutil.ReadAll(configFile)
	if err != nil {
		return err
	}

	err = yaml.Unmarshal(configData, config)
	if err != nil {
		return err
	}

	return nil
}
