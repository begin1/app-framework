package framework

import (
	"context"
	"fmt"
	"net/http"
	_ "net/http/pprof"
)

const (
	// 规定pprof端口，以便采集
	DefaultPProfPort = 8089
)

// 任务服务器
type PProfServer struct {
	// 配置文件
	config *Config
}

// 实现ServerInterface
func (svr *PProfServer) ShutDown(ctx context.Context) (err error) {
	return nil
}

// 实现ServerInterface
func (svr *PProfServer) Start(c *Config, svc ServiceInterface) {
	svr.config = c

	go func() {
		err := http.ListenAndServe(fmt.Sprintf("0.0.0.0:%d", DefaultPProfPort), nil)
		if err != nil {
			panic(err)
		}
	}()
}

// 实现ServerInterface
func (svr *PProfServer) Name() string {
	return "pprof"
}
